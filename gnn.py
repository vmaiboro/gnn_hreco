import numpy as np
import os
import json
import math
import time
import matplotlib.pyplot as plt
import seaborn as sns

from torch_geometric.data import Data
from torch_geometric.loader import DataLoader

import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.utils.data as data
import torch.optim as optim

from torch_geometric.nn import SAGEConv, to_hetero
import torch_geometric.nn as geom_nn
import torch_geometric.data as geom_data
import pytorch_lightning as pl
from pytorch_lightning.callbacks import LearningRateMonitor, ModelCheckpoint
from torchmetrics.classification import MulticlassConfusionMatrix


trainloader = torch.load("/sps/atlas/v/vmaiboro/gnn_me/data/graphs/trainloader.pt")
valloader   = torch.load("/sps/atlas/v/vmaiboro/gnn_me/data/graphs/valloader.pt")
#trainloader = torch.load("/sps/atlas/v/vmaiboro/gnn_me/train_smoller.pt")
#valloader   = torch.load("/sps/atlas/v/vmaiboro/gnn_me/val_smoller.pt")
print(trainloader.batch_size)

#print(trainloader.dataset[0]["Lepton"].x)
#print(type(trainloader.dataset[0]))
train = [het.to_homogeneous(node_attrs=["x"]) for het in trainloader.dataset]
val   = [het.to_homogeneous(node_attrs=["x"]) for het in valloader.dataset]
num = int(0.5*len(val))
test = val[:num]
val = val[num:]

trainloader = DataLoader(train, batch_size=1000)
valloader   = DataLoader(val,   batch_size=500)
testloader  = DataLoader(val,   batch_size=500)
print(len(valloader))
print(len(testloader))

ys = []
for data in trainloader:
    for i in range(len(data.y)):
        ys.append(data.y[i].item())
#print(type(ys[0]))
values, counts = np.unique(ys, return_counts=True)
for i in zip(values, counts):
    print(i)
tot = sum(counts)
weights = [1./(float(count)/tot) for count in counts]
print(weights)
print(len(valloader))
print(trainloader.dataset[0])
print(type(trainloader.dataset[0].x))

# Path to the folder where the pretrained models are saved
CHECKPOINT_PATH = "."
device = torch.device("cuda:0") if torch.cuda.is_available() else torch.device("cpu")
print(device)

gnn_layer_by_name = {
    "GCN": geom_nn.GCNConv,
    "GAT": geom_nn.GATConv,
    "GraphConv": geom_nn.GraphConv
}

class GNNModel(nn.Module):
    
    def __init__(self, c_in, c_hidden, c_out, num_layers=2, layer_name="GAT", dp_rate=0.1, **kwargs):
        """
        Inputs:
            c_in - Dimension of input features
            c_hidden - Dimension of hidden features
            c_out - Dimension of the output features. Usually number of classes in classification
            num_layers - Number of "hidden" graph layers
            layer_name - String of the graph layer to use
            dp_rate - Dropout rate to apply throughout the network
            kwargs - Additional arguments for the graph layer (e.g. number of heads for GAT)
        """
        super().__init__()
        gnn_layer = gnn_layer_by_name[layer_name]
        
        layers = []
        in_channels, out_channels = c_in, c_hidden
        for l_idx in range(num_layers-1):
            layers += [
                gnn_layer(in_channels=in_channels, 
                          out_channels=out_channels,
                          **kwargs),
                nn.ReLU(inplace=True),
                nn.Dropout(dp_rate)
            ]
            in_channels = c_hidden
        layers += [gnn_layer(in_channels=in_channels, 
                             out_channels=c_out,
                             **kwargs)]
        self.layers = nn.ModuleList(layers)
    
    def forward(self, x, edge_index):
        """
        Inputs:
            x - Input features per node
            edge_index - List of vertex index pairs representing the edges in the graph (PyTorch geometric notation)
        """
        for l in self.layers:
            # For graph layers, we need to add the "edge_index" tensor as additional input
            # All PyTorch Geometric graph layer inherit the class "MessagePassing", hence
            # we can simply check the class type.
            if isinstance(l, geom_nn.MessagePassing):
                x = l(x, edge_index)
            else:
                x = l(x)
        return x

class GraphGNNModel(nn.Module):
    
    def __init__(self, c_in, c_hidden, c_out, dp_rate_linear=0.5, **kwargs):
        """
        Inputs:
            c_in - Dimension of input features
            c_hidden - Dimension of hidden features
            c_out - Dimension of output features (usually number of classes)
            dp_rate_linear - Dropout rate before the linear layer (usually much higher than inside the GNN)
            kwargs - Additional arguments for the GNNModel object
        """
        super().__init__()
        self.GNN = GNNModel(c_in=c_in, 
                            c_hidden=c_hidden, 
                            c_out=c_hidden, # Not our prediction output yet!
                            **kwargs)
        self.head = nn.Sequential(
            nn.Dropout(dp_rate_linear),
            nn.Linear(c_hidden, c_out)
        )

    def forward(self, x, edge_index, batch_idx):
        """
        Inputs:
            x - Input features per node
            edge_index - List of vertex index pairs representing the edges in the graph (PyTorch geometric notation)
            batch_idx - Index of batch element for each node
        """
        x = self.GNN(x, edge_index)
        x = geom_nn.global_mean_pool(x, batch_idx) # Average pooling
        x = self.head(x)
        return x

class GraphLevelGNN(pl.LightningModule):
    
    def __init__(self, **model_kwargs):
        super().__init__()
        # Saving hyperparameters
        self.save_hyperparameters()
        
        self.model = GraphGNNModel(**model_kwargs)
        self.loss_module = nn.CrossEntropyLoss(weight=torch.Tensor(weights))#torch.tensor([1, 0.55, 0.7, 1.5, 2, 2]))
        self.confmatrix = MulticlassConfusionMatrix(num_classes=6)

    def forward(self, data, mode="train"):
        x, edge_index, batch_idx = data.x, data.edge_index, data.batch
        x = self.model(x.to(torch.float), edge_index.to(torch.long), batch_idx.to(torch.long))
        x = x.squeeze(dim=-1)
        
        if self.hparams.c_out == 1:
            preds = (x > 0).float()
            data.y = data.y.float()
        else:
            preds = x.argmax(dim=-1)
        loss = self.loss_module(x.to(torch.float), data.y.to(torch.long))
        #print(x.shape)
        #print(data.y.shape)
        acc = (preds == data.y).sum().float() / preds.shape[0]
        
        if mode=="test":
            #print(preds, data.y)
            return loss, acc, preds, data.y
        return loss, acc
    
    def configure_optimizers(self):
        optimizer = optim.AdamW(self.parameters(), lr=5e-4, weight_decay=0.0) # High lr because of small dataset and small model
        return optimizer

    def training_step(self, batch, batch_idx):
        loss, acc = self.forward(batch, mode="train")
        #self.log('train_loss', loss)
        self.log('train_acc', acc, prog_bar=True, on_step=False, on_epoch=True)
        self.log("train_loss", loss, prog_bar=True, on_step=True, on_epoch=True)
        return loss
    
    #def training_epoch_end(self, outputs) -> None:
    #    loss = sum(output['loss'] for output in outputs) / len(outputs)
    #    print(loss)


    def validation_step(self, batch, batch_idx):
        _, acc = self.forward(batch, mode="val")
        self.log('val_acc', acc)

    #def test_step(self, batch, batch_idx):
    #    _, acc = self.forward(batch, mode="test")
    #    self.log('test_acc', acc)
        
    def test_step(self, batch, batch_idx):
        _, acc, preds, truth = self.forward(batch, mode="test")
        self.log('test_acc', acc)
        self.confmatrix.update(preds, truth)
        
        # Plot and save confusion matrix if it's the last batch
        if batch_idx == 9:#1884:
            fig, ax = plt.subplots(figsize=(8, 6))
            cm = self.confmatrix.compute().cpu().numpy()
            #cm = cm / cm.sum(axis=0)[: , np.newaxis]
            classes = ['class_0', 'class_1', 'class_2', 'class_3', 'class_4', 'class_5']
            sns.heatmap(cm, annot=True,  cmap="viridis").invert_yaxis()
            plt.xlabel("True STXS bin")
            plt.ylabel("Reconstructed STXS bin")
            #im = ax.imshow(cm, cmap=plt.cm.Blues)
            #ax.set_title("Confusion matrix")
            #tick_marks = np.arange(len(classes))
            #ax.set_xticks(tick_marks)
            #ax.set_xticklabels(classes, rotation=45)
            #ax.set_yticks(tick_marks)
            #ax.set_yticklabels(classes)
            plt.savefig('foo_500pl200_5.png')


def train_graph_classifier(model_name, **model_kwargs):
    pl.seed_everything(42)
    
    # Create a PyTorch Lightning trainer with the generation callback
    root_dir = os.path.join(CHECKPOINT_PATH, "GraphLevel" + model_name)
    os.makedirs(root_dir, exist_ok=True)
    
    trainer = pl.Trainer(default_root_dir=root_dir,
                         callbacks=[ModelCheckpoint(save_weights_only=True, mode="max", monitor="val_acc")],
                         #gpus=1 if str(device).startswith("cuda") else 0,
                         devices = "auto",
                         max_epochs=200,
                         #progress_bar_refresh_rate=0
                         )
    trainer.logger._default_hp_metric = None # Optional logging argument that we don't need

    # Check whether pretrained model exists. If yes, load it and skip training
    #pretrained_filename = "/sps/atlas/v/vmaiboro/gnn_me/GraphLevelGAT/lightning_logs/version_21/checkpoints/epoch=388-step=15560.ckpt"#os.path.join(CHECKPOINT_PATH, f"GraphLevel{model_name}.ckpt")
    #pretrained_filename = "/sps/atlas/v/vmaiboro/gnn_me/GraphLevelGAT/lightning_logs/version_24/checkpoints/epoch=162-step=6520.ckpt"
    #pretrained_filename = "/sps/atlas/v/vmaiboro/gnn_me/GraphLevelGAT/lightning_logs/version_25/checkpoints/epoch=42-step=1720.ckpt"
    #pretrained_filename = "/sps/atlas/v/vmaiboro/gnn_me/GraphLevelGAT/lightning_logs/version_28/checkpoints/epoch=133-step=5360.ckpt"
    pretrained_filename = "/sps/atlas/v/vmaiboro/gnn_me/GraphLevelGAT/lightning_logs/version_30/checkpoints/epoch=0-step=40.ckpt"
    
    #if os.path.isfile(pretrained_filename):
    #    print("Found pretrained model, loading...")
    #    model = GraphLevelGNN.load_from_checkpoint(pretrained_filename)
    #else:
    pl.seed_everything(42)
    #model = GraphLevelGNN(c_in=4, 
    #                      c_out=6, 
    #                      **model_kwargs)
    model = GraphLevelGNN.load_from_checkpoint(pretrained_filename)
    trainer.fit(model, trainloader, valloader)
    model = GraphLevelGNN.load_from_checkpoint(trainer.checkpoint_callback.best_model_path)
    # Test best model on validation and test set
    #train_result = trainer.test(model, dataloaders=trainloader)
    test_result = trainer.test(model, dataloaders=valloader)
    result = {"test": test_result[0]['test_acc']}#, "train": train_result[0]['test_acc']} 
    return model, result

model, result = train_graph_classifier(model_name="GAT", 
                                       c_hidden=500, 
                                       layer_name="GAT", 
                                       num_layers=5, 
                                       dp_rate_linear=0.1,
                                       dp_rate=0.1)

#print(f"Train performance: {100.0*result['train']:4.2f}%")
print(f"Test performance:  {100.0*result['test']:4.2f}%")