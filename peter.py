import numpy as np
import os
import json
import math
import time
import matplotlib.pyplot as plt
import seaborn as sns

from torch_geometric.data import Data
from torch_geometric.loader import DataLoader

import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.utils.data as data
import torch.optim as optim

from torch_geometric.nn import SAGEConv, to_hetero
import torch_geometric.nn as geom_nn
import torch_geometric.data as geom_data
import pytorch_lightning as pl
from pytorch_lightning.callbacks import LearningRateMonitor, ModelCheckpoint
from torchmetrics.classification import MulticlassConfusionMatrix

from torch.nn import Sequential as Seq, Linear as Lin, ReLU, LazyLinear, LayerNorm
from torch_geometric.nn import MetaLayer

from torch_scatter import scatter_mean



trainloader = torch.load("/sps/atlas/v/vmaiboro/gnn_me/train_smoller.pt")
valloader   = torch.load("/sps/atlas/v/vmaiboro/gnn_me/val_smoller.pt")
print(trainloader.batch_size)
#print(len(valloader))
print(trainloader.dataset[0])
#print(type(trainloader.dataset[0]))
#train = [het for het in trainloader.dataset]
#val   = [het for het in valloader.dataset]
#trainloader = DataLoader(train, batch_size=1)
#valloader   = DataLoader(val,   batch_size=1)
#print(len(valloader))
#print(trainloader.dataset[0])
#print(type(trainloader.dataset[0].x))

# Path to the folder where the pretrained models are saved
CHECKPOINT_PATH = "."
device = torch.device("cuda:0") if torch.cuda.is_available() else torch.device("cpu")
print(device)

class EdgeModel(torch.nn.Module):
    def __init__(self):
        super().__init__()
        self.edge_mlp = Seq(LazyLinear(256), ReLU(), LazyLinear(256), ReLU(), LayerNorm(256))

    def forward(self, src, dest, edge_attr, u, batch):
        # src, dest: [E, F_x], where E is the number of edges.
        # edge_attr: [E, F_e]
        # u: [B, F_u], where B is the number of graphs.
        # batch: [E] with max entry B - 1.
        #print(src.shape)
        #print(dest.shape)
        #print(edge_attr.shape)
        #print(u[batch].shape)
        out = torch.cat([src, dest, edge_attr, u[batch]], 1).to(torch.float)
        return self.edge_mlp(out)

class NodeModel(torch.nn.Module):
    def __init__(self):
        super().__init__()
        self.node_mlp_1 = Seq(LazyLinear(256), ReLU(), LazyLinear(256), ReLU(), LayerNorm(256))
        self.node_mlp_2 = Seq(LazyLinear(256), ReLU(), LazyLinear(256), ReLU(), LayerNorm(256))

    def forward(self, x, edge_index, edge_attr, u, batch):
        # x: [N, F_x], where N is the number of nodes.
        # edge_index: [2, E] with max entry N - 1.
        # edge_attr: [E, F_e]
        # u: [B, F_u]
        # batch: [N] with max entry B - 1.
        row, col = edge_index
        out = torch.cat([x[row], edge_attr], dim=1)
        out = self.node_mlp_1(out)
        out = scatter_mean(out, col, dim=0, dim_size=x.size(0))#reduce='mean', 
        out = torch.cat([x, out, u[batch]], dim=1).to(torch.float)
        return self.node_mlp_2(out)

class GlobalModel(torch.nn.Module):
    def __init__(self):
        super().__init__()
        self.global_mlp = Seq(LazyLinear(512), ReLU(), 
                              LazyLinear(256), ReLU(),
                              LazyLinear(128), ReLU(),
                              LazyLinear(64), ReLU(),
                              LazyLinear(32), ReLU(),
                              LazyLinear(6), ReLU(),
                              LayerNorm(6))

    def forward(self, x, edge_index, edge_attr, u, batch):
        # x: [N, F_x], where N is the number of nodes.
        # edge_index: [2, E] with max entry N - 1.
        # edge_attr: [E, F_e]
        # u: [B, F_u]
        # batch: [N] with max entry B - 1.
        #print(u.shape)
        #print(scatter_mean(x, batch, dim=0).shape)
        #print(x.shape, batch.shape)
        out = torch.cat([
            u,
            scatter_mean(x, batch, dim=0),#, reduce='mean'
        ], dim=1).to(torch.float)
        return self.global_mlp(out)
    
class GraphModel(torch.nn.Module):
    def __init__(self):
        super().__init__()
        self.meta = MetaLayer(EdgeModel(), NodeModel(), GlobalModel())
        self.graph_mlp = LazyLinear(6)

    def forward(self, x, edge_index, edge_attr, u, batch):
        # x: [N, F_x], where N is the number of nodes.
        # edge_index: [2, E] with max entry N - 1.
        # edge_attr: [E, F_e]
        # u: [B, F_u]
        # batch: [N] with max entry B - 1.
        #print(u.shape)
        #print(scatter_mean(x, batch, dim=0).shape)
        #print(x.shape, batch.shape)
        _, _, out = self.meta(x, edge_index, edge_attr, u, batch)
        return self.graph_mlp(out)

model = GraphModel()#.to(device)#MetaLayer(EdgeModel(), NodeModel(), GlobalModel())

criterion = torch.nn.CrossEntropyLoss(weight=torch.tensor([4.9678391959798995, 2.9440142942227516, 3.6733859730608454, 7.896166134185304, 20.671197072660743, 84.85836909871244]))
optimizer = optim.AdamW(model.parameters(), lr=1e-3, weight_decay=0.0)

for epoch in range(100):
    for i, data in enumerate(trainloader):
        optimizer.zero_grad()
        #print("u: ", data.u.shape)
        out = model(data.x.to(torch.float), data.edge_index.to(torch.long), data.edge_attr, data.u, data.batch.to(torch.long))
        #print("targ: ", data.y.shape)
        loss = criterion(out, data.y.to(torch.long))
        loss.backward()
        optimizer.step()
    print(f'Epoch {epoch}: Loss={loss.item()}')

# Evaluate the model
model.eval()
correct = 0
total = 0
preds = 0
truth = 0

for data in valloader:
    out = model(data.x.to(torch.float), data.edge_index.to(torch.long), data.edge_attr, data.u, data.batch.to(torch.long))
    pred = out.argmax(dim=1)
    total += data.num_graphs
    correct += (pred == data.y).sum().float()
print(pred)
accuracy = correct / total
print(f'Test Accuracy={accuracy:.2f}')
