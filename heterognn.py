import numpy as np
import os
import json
import math
import time
import matplotlib.pyplot as plt
import seaborn as sns

from torch_geometric.data import Data
from torch_geometric.loader import DataLoader

import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.utils.data as data
import torch.optim as optim

from torch_geometric.nn import SAGEConv, to_hetero, GATConv, global_mean_pool
import torch_geometric.nn as geom_nn
import torch_geometric.data as geom_data
import pytorch_lightning as pl
from pytorch_lightning.callbacks import LearningRateMonitor, ModelCheckpoint
from torchmetrics.classification import MulticlassConfusionMatrix


trainloader = torch.load("/sps/atlas/v/vmaiboro/gnn_me/data/graphs/trainloader.pt")
valloader   = torch.load("/sps/atlas/v/vmaiboro/gnn_me/data/graphs/valloader.pt")
print(trainloader.batch_size)
print(len(valloader))
print(trainloader.dataset[0]["Lepton"].x)
#print(type(trainloader.dataset[0]))
#train = [het for het in trainloader.dataset]
#val   = [het for het in valloader.dataset]
#trainloader = DataLoader(train, batch_size=1)
#valloader   = DataLoader(val,   batch_size=1)
#print(len(valloader))
#print(trainloader.dataset[0])
#print(type(trainloader.dataset[0].x))

# Path to the folder where the pretrained models are saved
CHECKPOINT_PATH = "."
device = torch.device("cuda:0") if torch.cuda.is_available() else torch.device("cpu")
print(device)

class GraphGNNModel(nn.Module):
    def __init__(self):
        super(GraphGNNModel, self).__init__()
        self.conv1 = SAGEConv(4, 150, normalize=True)
        self.conv2 = SAGEConv(150, 300, normalize=True)
        self.conv3 = SAGEConv(300, 100, normalize=True)
        self.fc1 = nn.Linear(100, 50)
        self.fc2 = nn.Linear(50, 6)
    
    def forward(self, x_dict, edge_index_dict):
        
        x = torch.cat([x_dict['Lepton'], x_dict['Met'], x_dict['Jet']], dim=0)
        #print("input: ", x.shape)
        x = F.elu(self.conv1(x, edge_index_dict[('Lepton', 'Lepton_to_Jet', 'Jet')]))
        #print("input: ", x.shape)
        x = F.elu(self.conv2(x, edge_index_dict[('Jet', 'Jet_to_Jet', 'Jet')]))
        x = F.elu(self.conv3(x, edge_index_dict[('Jet', 'Jet_to_Met', 'Met')]))
        #print("input: ", x.shape)
        x = self.fc1(x)
        batch = torch.cat([torch.full((x_dict['Lepton'].size(0),), 0, dtype=torch.long),
                        torch.full((x_dict['Met'].size(0),), 1, dtype=torch.long),
                        torch.full((x_dict['Jet'].size(0),), 2, dtype=torch.long)], dim=0)
        #print("input: ", x.shape)
        #print("batch: ", batch.shape)
        x = global_mean_pool(x, batch, size=x.shape[0]//5)  # Aggregation over all nodes
        #print("input: ", x.shape)
        
        x = self.fc2(x)
        #print("input: ", x.shape)
        return x


model = GraphGNNModel()

criterion = torch.nn.CrossEntropyLoss(weight=torch.tensor([0.9, 0.5, 0.7, 1.5, 2, 2]))
optimizer = optim.AdamW(model.parameters(), lr=1e-1, weight_decay=0.0)

for epoch in range(50):
    for i, data in enumerate(trainloader):
        optimizer.zero_grad()
        out = model(data.x_dict, data.edge_index_dict)
        #print("targ: ", data.y.shape)
        loss = criterion(out, data.y.to(torch.long))
        loss.backward()
        optimizer.step()
    print(f'Epoch {epoch}: Loss={loss.item()}')

# Evaluate the model
model.eval()
correct = 0
total = 0
preds = 0
for data in valloader:
    out = model(data.x_dict, data.edge_index_dict)
    pred = out.argmax(dim=1)
    total += data.num_graphs
    correct += (pred == data.y).sum().float()
print(pred)
accuracy = correct / total
print(f'Test Accuracy={accuracy:.2f}')

